#!/bin/bash

#cd into dependencies directory for fetching project dependencies
_dev_dependencies_directory(){ (
  unset CDPATH
  SOURCE="${BASH_SOURCE[0]}"
  while test -L "$SOURCE";do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    test "$(printf %.1s "$SOURCE")" != "/" && SOURCE="$DIR/$SOURCE"
  done
  cd -P "$( dirname "$SOURCE" )" && pwd
) }
cd "$(_dev_dependencies_directory)"

################################################################################
#                       Requirements for contributing                          #
################################################################################
## uncrustify
### Required: for standardization of code amoungst all collaborators
### homepage: http://uncrustify.sourceforge.net/
### github: https://github.com/uncrustify/uncrustify
### Install directions: consult INSTALL and README.md

git submodule add https://github.com/uncrustify/uncrustify.git
