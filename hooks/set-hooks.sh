#!/bin/bash

_dev_tool_directory(){ (
  unset CDPATH
  SOURCE="${BASH_SOURCE[0]}"
  while test -L "$SOURCE";do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    test "$(printf %.1s "$SOURCE")" != "/" && SOURCE="$DIR/$SOURCE"
  done
  cd -P "$( dirname "$SOURCE" )"/.. && pwd
) }

DEVTOOLPATH=$(_dev_tool_directory)
DEVHOOKPATH="${DEVTOOLPATH}/hooks"
GITPATH=$(git rev-parse --git-dir)
GITHOOKPATH="${GITPATH}/hooks"

HOOKS=$(find "$DEVHOOKPATH" -type f -name '*[^\.sh]')
for hook in $HOOKS;do
  bakhook="${hook}.bak"
  cp "$hook" "$bakhook"
  sed -e "s,DEVTOOLPATH=.*,DEVTOOLPATH=\"$DEVTOOLPATH\"," "$bakhook" > "$bakhook".tmp
  mv -- "$bakhook".tmp "$bakhook"
  sed -e "s,GITPATH=.*,GITPATH=\"$GITPATH\"," "$bakhook" > "$bakhook".tmp
  mv -- "$bakhook".tmp "$bakhook"
  cp -v "$bakhook" "$GITHOOKPATH"/"${hook##*/}"
  chmod +x "$GITHOOKPATH"/"${hook##*/}"
  rm "$bakhook"
done

#cp -v "$DEVHOOKPATH"/pre-commit.tmp "$GITHOOKPATH"/pre-commit
#cp -v "$DEVHOOKPATH"/prepare-commit-msg.tmp "$GITHOOKPATH"/prepare-commit-msg
#cp -v "$DEVHOOKPATH"/commit-msg.tmp "$GITHOOKPATH"/commit-msg
#cp -v "$DEVHOOKPATH"/post-commit.tmp "$GITHOOKPATH"/post-commit
