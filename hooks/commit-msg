#!/bin/bash

DEVTOOLPATH="./dev_tools"
GITPATH="./.git"

#restore to merge state if applicable
mergeRestore() {
  echo "no-ff" > "$GITPATH"/MERGE_MODE
  echo "$MERGE" > "$GITPATH"/MERGE_HEAD
  mv MERGE_MSG.tmp "$GITPATH"/MERGE_MSG
}

#in case of failure, restore to state just prior to attempting a commit
gitRestore() {
MERGE=$(git rev-parse MERGE_HEAD 2> /dev/null)
if test "$MERGE" != "MERGE_HEAD"; then
  cp "$GITPATH"/MERGE_MSG MERGE_MSG.tmp
fi
if test "$(git stash list | grep -c "hook:")" != 0; then
  git stash save -q "orginal index"
  git stash apply -q --index stash@{1}
  git stash drop -q; git stash drop -q
fi
if test "$MERGE" != "MERGE_HEAD"; then
  mergeRestore
fi
#remove temporary standardized files
rm -r "$DEVTOOLPATH"/tmp
}

#begin reviewing commit message

i=0
#ref_flag=0
SUBJECTS="\(chor:\|dbug:\|docs:\|feat:\|init:\|merg:\|rfct:\|styl:\|test:\)"
#REFS="\(close\|closes\|closed\|fix\|fixes\|fixed\|resolve\|resolves\|resolved\)"
#((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing))(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?:, *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)

while read -r line; do
  #skip commented out lines
  [ "${line:0:1}" = "#" ] && continue

  #check that first line meets subject requirements
  if test $i -eq 0 && [[ $line =~ [^[:space:]] ]]; then
    #subject should begin with 4 char. classifier
    if test "$(head -n 1 "$1" | cut -c 1-5 | grep -c $SUBJECTS)" == 0; then
cat <<\EOF
Error: Your commit tile did not start with a subject. Use any of the following:
       chor:, dbug:, docs:, feat:, init:, merg:, rfct:, styl:, or test:.
EOF
      gitRestore
      exit 1
    fi
    #subject should be less than 50 characters
    if test ${#line} -gt 50; then
cat <<\EOF
Error: Your subject was longer than 50 char.
EOF
      gitRestore
      exit 1
    fi
  fi

  #require that subject is seperated from body by blank line
  if test $i -eq 1;then
    if [[ ! $line =~ [^[:space:]] ]]; then
      i=$((i+1))
      continue
    else
cat <<\EOF
Error: Your subject and body must be seperated by a blank line.
EOF
      gitRestore
      exit 1
    fi
  fi

  #skip counting lines without substance
  [[ ! $line =~ [^[:space:]] ]] && continue
  i=$((i+1))

  #check body meets requirements
  if test "$i" -gt 1; then
    # body must be less than 73 charaters
    if test ${#line} -gt 72; then
cat <<\EOF
Error: Your commit body was longer than 72 char.
EOF
      gitRestore
      exit 1
    fi
  fi
done < "$1"

#there must have been a body
if test $i -lt 3;then
cat <<\EOF
Error: Your commit has no body. Make an effort to be descriptive.
EOF
  gitRestore
  exit 1
fi
