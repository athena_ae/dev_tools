#!/bin/bash

_dev_tool_directory(){ (
  unset CDPATH
  SOURCE="${BASH_SOURCE[0]}"
  while test -L "$SOURCE";do
    DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    test "$(printf %.1s "$SOURCE")" != "/" && SOURCE="$DIR/$SOURCE"
  done
  cd -P "$( dirname "$SOURCE" )" && pwd
) }
DEVTOOLPATH=$(_dev_tool_directory)

# set hooks to those from git_hooks
"$DEVTOOLPATH"/hooks/set-hooks.sh

# --no-ff and --no-commit for dev and master branches
git config --local branch.master.mergeoptions "--no-ff"
git config --local branch.dev.mergeoptions "--no-ff"

# enable merge ours for gitattribute merge=ours, only locally
git config --local merge.ours.driver true

# Stop from pushing a submodule that itself has not yet been pushed
git config --local push.recurseSubmodules check
