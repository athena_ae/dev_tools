#!/bin/bash

## Add standard sources to look for uncrustify
. ~/.bashrc; . ~/.bash_profile; . ~/.profile

## Find uncrustify or its possible alias
unc=$(command -v uncrustify)
if [ -z "$unc" ]; then
  unc="$(alias | grep "uncrustify" | cut -d "'" -f2 | cut -d "\"" -f2)"
fi
if [ -z "$unc" ]; then
cat <<\EOF
Error: Cannot find uncrustify.
       Need $(command -v uncrustify) to be non-null or have an uncrustify alias.
       Sourced ~/.bashrc, ~/.bash_profile and ~/.profile for aliases.
       You can alter dev_tools/standardize/standardize.sh to source other
       profiles.
EOF
exit 1
fi

## Find all .c and .h files you will be committing
#### Options to standardize all (if true;) or only committing (if false;)
#### Should standardize all, but saves time to not check non-commits
#### after a universal standardization has been run once.
#### Need to be wary of users who commit without these hooks.
if false; then
  files=$(git ls-files | grep "\.[ch]$")
else
  files=$(git diff --cached --name-only | grep "\.[ch]$")
fi

## Standardize files
for item in $files ; do
  path="/${item}" && path=${path%/*} && path=${path:1:${#path}}
  mkdir -p dev_tools/tmp/unc/old/"$path"
  mkdir -p dev_tools/tmp/unc/new/"$path"
  cp "$item" dev_tools/tmp/unc/old/"$path"
  #edit once
  if test "$($unc -q -c dev_tools/standards/athena.cfg -f dev_tools/tmp/unc/old/"$item" --if-changed | wc -c)" -gt 0;then
    $unc -c dev_tools/standards/athena.cfg -f dev_tools/tmp/unc/old/"$item" -o dev_tools/tmp/unc/new/"$item"
  fi
  #recursive edit
  while test "$($unc -q -c dev_tools/standards/athena.cfg -f dev_tools/tmp/unc/new/"$item" --if-changed | wc -c)" -gt 0; do
    $unc -c dev_tools/standards/athena.cfg -f dev_tools/tmp/unc/new/"$item" -o dev_tools/tmp/unc/new/"$item"
  done
done

## Human confirmation of standardizations
#### Some external mergetools, e.g., opendiff, uncontrollably load
#### multiple environments as they do not block. A patch is to wait
#### for some output and pipe it to cat.
mergetool=$(git config --list | grep "merge.tool" | cut -d "=" -f2)
for item in $files ; do
#  if test "$($unc -q -c dev_tools/standards/athena.cfg -f "$item" --if-changed | wc -c)" -gt 0; then
  if test -f dev_tools/tmp/unc/new/"$item";then
    $mergetool "$item" dev_tools/tmp/unc/new/"$item" < /dev/tty > /dev/tty | wc -c > /dev/null 2>&1
  fi
done
